package Mediator.EX1.components;

import Mediator.EX1.mediator.Mediator;

public interface Component {
    void setMediator(Mediator mediator);
    String getName();
}
